import 'package:currencies/screens/analytics_screen.dart';
import 'package:currencies/screens/history_screen.dart';
import 'package:flutter/material.dart';
import 'package:currencies/screens/home_screen.dart';
import 'package:currencies/screens/change_screen.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ozhogin currencies',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.lime),
        useMaterial3: true,
        textTheme: const TextTheme(
          bodyMedium: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      routes: {
        '/': (context) => const HomeScreen(title: 'Ozhogin currencies'),
        '/change': (context) => const ChangeScreen(),
        '/history': (context) => const HistoryScreen(),
        '/analytics': (context) => const AnalyticsScreen(),
      },
      initialRoute: '/',
    );
  }
}
