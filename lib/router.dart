// import 'package:auto_route/auto_route.dart';
// import 'package:auto_route/annotations.dart';
// import 'package:currencies/screens/history_screen.dart';
// import 'package:currencies/screens/home_screen.dart';
//
//
// @AutoRouterConfig()
// class AppRouter extends $AppRouter {
//
//   @override
//   RouteType get defaultRouteType => RouteType.material();
//
//   @override
//   List<AutoRoute> get routes => [
//     AutoRoute(
//       path: '/',
//       page: HomeScreen,
//     ),
//     AutoRoute(
//       path: 'history',
//       name: 'HistoryRouter',
//       page: HistoryScreen,
//     ),
//     AutoRoute(
//       path: 'analytics',
//       name: 'AnalyticsRouter',
//       page: AnalyticsScreen,
//     )
//   ];
// }
//
// class $AppRouter {}