import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class ChangeScreen extends StatefulWidget {
  const ChangeScreen({super.key});

  @override
  State<ChangeScreen> createState() => _ChangeScreenState();
}

class _ChangeScreenState extends State<ChangeScreen> {
  Map? currency;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _firstInputController = TextEditingController(text: "1");
  TextEditingController _secInputController = TextEditingController();
  String _transferedCurrency = '';
  String _firstCurrency = '';

  _changeCurrencyValue(){
    setState(() {
      num cur = int.parse(_firstInputController.text);
      num doubleCur = num.parse((cur * currency!["price"]).toStringAsFixed(2));
      _transferedCurrency = doubleCur.toString();
      _secInputController.text = _transferedCurrency;
    });
  }

  _changeFirstCurrencyValue(){
    setState(() {
      num cur = int.parse(_secInputController.text);
      num doubleCur = num.parse((cur / currency!["price"]).toStringAsFixed(2));
      _firstCurrency = doubleCur.toString();
      _firstInputController.text = _firstCurrency;
    });
  }

  @override
  void initState() {
    super.initState();
    _firstInputController.addListener(_changeCurrencyValue);
    _secInputController.addListener(_changeFirstCurrencyValue);
  }

  @override
  void dispose() {
    _firstInputController.dispose();
    _secInputController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    final args = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    currency = args;
    _secInputController.text = currency!["price"].toString();
    setState(() {});
    super.didChangeDependencies();
  }

  void change() {

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Currency change')),
        body: Padding(
            padding: EdgeInsets.only(top:10, left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // Container(
                //   child: Text(
                //     '1 ' + currency?["name"] + ' = ' + currency!["price"].toString() + ' RUB',
                //     textDirection: TextDirection.ltr,
                //     textAlign: TextAlign.center,
                //     style: TextStyle(
                //       fontSize: 28
                //     ),
                //   ),
                // ),
                //Text("$_transferedCurrency"),
                Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextField(
                          style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.w500,
                          ),
                          controller: _firstInputController,
                          decoration: InputDecoration(
                              labelText: currency?["name"],
                              labelStyle: TextStyle(fontSize: 36),
                          ),
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                        TextField(
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.w500,
                          ),
                          controller: _secInputController,
                          decoration: InputDecoration(
                              labelText: "RUB",
                              labelStyle: TextStyle(fontSize: 36)
                          ),
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 25),
                          child: ElevatedButton(
                            style: ButtonStyle(
                              padding: MaterialStateProperty.all<EdgeInsets>(
                                  EdgeInsets.all(10)
                              ),
                            ),
                            onPressed: () {
                              change();
                              Navigator.of(context).pushNamed(
                                '/',
                                arguments: [],
                              );
                            },
                            child: const Center(
                                child: Text(
                                  'Обменять',
                                  style: TextStyle(fontSize: 28),
                                )
                            ),
                          ),
                        )
                      ],
                    )
                )
              ],
            ),
        )
    );
  }
}
