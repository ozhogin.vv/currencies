import 'package:flutter/material.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key, required this.title});

  final String title;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void likeCurrency(currency) {
      _likesCounter += 1;
      _currenciesList.remove(currency);
      currency["like"] = true;
      _currenciesList.insert(0, currency);
      setState(() {});
  }

  void unlikeCurrency(currency) {
    _likesCounter -= 1;
    _currenciesList.remove(currency);
    currency["like"] = false;
    _currenciesList.add(currency);
    setState(() {});
  }

  int _likesCounter = 0;

  List _currenciesList = [
    {
      "name": "RUB",
      "price": 1,
      "img": "rub.png",
      "like": false,
      "account": 999,
    },
    {
      "name": "USD",
      "price": 89.43,
      "img": "usd.jpg",
      "like": false,
      "account": 999,
    },
    {
      "name": "EUR",
      "price": 97.61,
      "img": "eur.png",
      "like": false,
      "account": 999,
    },
    {
      "name": "CNY",
      "price": 12.39,
      "img": "cny.png",
      "like": false,
      "account": 999,
    },
  ];
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text(widget.title),
        ),
        body: ListView.separated(
          padding: EdgeInsets.only(top:4),
          itemCount: _currenciesList.length,
          separatorBuilder: (context, index) => Divider(),
          itemBuilder: (context, i) {
            final currency = _currenciesList[i];
            return ListTile(
              leading: Image(image: AssetImage('assets/' + currency["img"]), height: 40, width: 40,),
              title: Text(
                currency["name"],
                style: theme.textTheme.bodyMedium,
              ),
              trailing: IconButton(
                  onPressed: () {
                    if (currency["like"] == false) {
                      likeCurrency(currency);
                    } else {
                      unlikeCurrency(currency);
                    }
                  },
                  icon: Icon((currency["like"] == false) ? Icons.star_border: Icons.star,),
              ),
              onTap: () {
                if ((currency["like"] == false)) {
                  _currenciesList.remove(currency);
                  _currenciesList.insert(_likesCounter, currency);
                } else {
                  _currenciesList.remove(currency);
                  _currenciesList.insert(0, currency);
                }
                setState(() {});
                Navigator.of(context).pushNamed(
                  '/change',
                  arguments: currency,
                );
              },
            );
          }
        ),
        bottomNavigationBar:
          SalomonBottomBar(
            margin: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 40,
            ),
            items: [
              SalomonBottomBarItem(
                selectedColor: Colors.black,
                icon: const Icon(
                Icons.change_circle,
                size: 30,
                ),
              title: const Text('Change'),
              ),
              SalomonBottomBarItem(
                selectedColor: Colors.blue[200],
                icon: const Icon(
                Icons.history,
                size: 30,
                ),
              title: const Text('History'),
              ),
              SalomonBottomBarItem(
                selectedColor: Colors.pinkAccent[100],
                icon: const Icon(
                Icons.analytics,
                size: 30,
              ),
              title: const Text('Analytics'),
              ),
            ],
        ),
    );
  }
}

// class BottomNabBar extends StatelessWidget {
//   const BottomNabBar({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return AutoTabsScaffold(
//       appBarBuilder: (_, tabsRouter) => AppBar(
//         backgroundColor: Colors.indigo,
//         title: const Text('FlutterBottomNav'),
//         centerTitle: true,
//         leading: const AutoBackButton(),
//       ),
//       backgroundColor: Colors.indigo,
//       routes: const [
//         '/': (context) => const HomeScreen(title: 'Ozhogin currencies'),
//         '/change': (context) => const ChangeScreen(),
//         '/history': (context) => const HistoryScreen(),
//         '/analytics': (context) => const AnalyticsScreen(),
//       ],
//       bottomNavigationBuilder: (_, tabsRouter) {},
//     );
//   }
// }